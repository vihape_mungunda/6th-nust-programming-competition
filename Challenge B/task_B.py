import re
import string
while True:
    inputs = input("Enter your information in the format - first name, date of birth (yyyy-mm-dd), postfix (With each input seperated by spaces): ")
    inputs_list = [x for x in inputs.split(' ')] # Vihape 2004-08-12 0000-5
    if len(inputs_list) != 3: # If too many or too little values are entered, it is rejected
        print("Input is missing values or there are too many (Enter only first name), try again")
        continue # To restart the loop
    name = inputs_list[0] # Vihape
    if len([x for x in name if x not in string.ascii_letters]) > 0: # Checks to see if there are any characters that are not a letter
        print("Invalid name entered, try again")
        continue
    dob = inputs_list[1] # 2004-08-12
    pattern = r'[1-2][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]' # The pattern the dob has to fit
    if re.match(pattern, dob) and 1800 <= int(dob[0:4]) <= 2021 and int(dob[5:7]) <= 12 and int(dob[8:10]) <= 31: # year cannot be less than 1800 or bigger than 2021
        pass # If dob fits pattern, then the loop continues
    else:
        print("Invalid Date of Birth entered, try again.")
        continue
    postfix = inputs_list[2] # 0000-5
    pattern = r'[0-9]{4}-[0-9]' # Inputted postfix has to fit this pattern, {4} means there can only be 4 digits before the -
    if re.match(pattern, postfix):
        pass
    else:
        print("Invalid postfix entered, try again")
        continue
    break


spitted_dob = dob.split('-') # [2004, 08, 12]
id = spitted_dob[0][2:4] + spitted_dob[1] + spitted_dob[2] # 04 + 08 + 12

passport_num = f'P-NAM {id} {postfix[3:6]}'
print(f'Good day {name}, welcome to the Ministry of Home affairs and Immigration automated system.')
print(f'Your ID number is : {id} {postfix}')
print(f'Passport code: {passport_num}')

