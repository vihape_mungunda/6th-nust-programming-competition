import string
def check_qualify(field, points, inputs):
    qualified = {}
    if field in requirements:
        if points >= requirements[field][0]:
            subjects = requirements[field][1]  # {'English': 'D', 'Mathematics': 'E'}
            comments = [] # WIll be the comments for a given field
            for subj in subjects:
                check_notin = False
                for st_subj in inputs:
                    if st_subj not in subjects:
                        qualified[field] = 'no'
                        comments.append(f'{st_subj} missing on list of subjects')
                        check_notin = True
                    if check_notin:
                        qualified[field] = comments
                        break

                if inputs[subj] in ['1', '2', '3', '4']: # Will compare digits if the symbol is a digit
                    if inputs[subj] <= subjects[subj]:
                        pass
                elif not inputs[subj] >= subjects[subj]: # Compares letters if the symbol is a letter
                    pass
                else:
                    comments.append(f'Symbol for {subj} is too low ({inputs[subj]}).')
            if len(comments) > 0:
                qualified[field] = comments
            else:
                qualified[field] = ''
    else:
        qualified[field] = None
    return qualified  #


def output(field, index, qual): #
    eligible_output = 'Congratulations you are eligible for tertiary studies in Namibia.'
    print(normal_output + eligible_output)
    check = qual[field]
    if check != '' and check != None: # checks to see If the learner fails a subject
        comments_subject = ' '.join(check)
        print(f'Field of Study {index}: Unfortunately you have not been admitted into {field}.\nComment: {comments_subject}')
    elif check is None: # Checks to see whether subject is missing for the chosen field or not
        print(f'Field of Study {index}: {field} is not offered at NUST')
    elif check == '': # Checks to see if learner is qualified for tertiary subject
        print(f'Field of study {index}: Congratulations you have been admitted into {field}!')




num_learners = int(input("Enter number of learners: "))
learners = {}
requirements = {'management sciences': [26, {'english': 'D', 'mathematics': 'E'}],
                'human sciences': [26, {'english': 'C'}], 'computing and informatics': [30, {'mathematics': 'C'}],
                'engineering sciences': [37, {'mathematics': '3', 'english': '4', 'physical science': '4'}],
                'health and applied sciences': [30, {'english': 'C', 'biology': 'D', 'mathematics': 'D',
                                                     'physical science': 'D'}],
                'natural resources and spatial sciences': [30, {'english': 'C', 'geography': 'D', 'mathematics': 'D'}]}
grading = {'1': 10, '2': 9, '3': 8, '4': 7, 'A+': 8, 'A': 7, 'B': 6,
           'C': 6, 'D': 4, 'E': 3, 'F': 2, 'G': 1, 'U': 0}
for y in range(num_learners):
    while True:
        name = input("\nEnter learner's full names: ")
        if len([x for x in name.split(' ') if
                x != ' ' and x != '']) == 1:  # Checking to see if there are consecutive spaces in the name
            print("Name must be in the format of 'Name Surname'")
        else:
            num_of_proper_letters = [x for x in name if
                                     x in string.ascii_letters]  # Stores the count for the amount of actual characters in name
            if len(num_of_proper_letters) == 0:  # If there are none, then the name is invalid
                print("Invalid Name, must contain characters")
            else:
                if len([x for x in name if
                        x not in string.ascii_letters + ' ']) == 0:  # Checks to see if there are characters that are neither a letter nor a space
                    correct_name = True  # If all letters are ok, then correct_name is True and that will break the loop
                else:
                    print("Invalid name, must not contain non-letter or non-space characters")
        if correct_name == True:
            break
    genders = {'male': 'Mr.', 'female': 'Ms./Mrs.'}
    while True:
        gender = input("Enter learner's gender: ")
        if gender.lower() not in genders:
            print('Invalid Gender entered, try again')
        else:
            break
    total_points = 0
    student_inputs = {}
    for x in range(5):
        while True:

            ask = input(f"Enter subject {x + 1} and symbol: ")
            subject = ' '.join([word.lower() for word in ask.split(' ') if word not in grading]) # Will contain only words that are not symbols
            symbol = ask.split(' ')[-1]

            if symbol not in grading:
                print('Invalid symbol entered, try again')
            elif symbol == '-':
                symbol = 'U'
                break
            else:
                break

        total_points += grading[symbol]
        student_inputs[subject] = symbol
        learners[name] = student_inputs
    fail = None
    if student_inputs['english'] == 'F':
        total_points = 0
        fail = 'Unfortunately you are not eligble for tertiary studies in Namibia, as you have received an "E" or less in English.'
    elif total_points < 25:
        fail = 'Unfortunately you are not eligble for tertiary studies in Namibia, as you have received less than 25 points.'
    else:
        field1 = input('Field of study 1: ').lower()
        f1 = check_qualify(field1, total_points, student_inputs)

        field2 = input('Field of study 2: ').lower()
        f2 = check_qualify(field1, total_points, student_inputs)

    normal_output = f'Dear {genders[gender]} {name}, you have {total_points} points in 5 subjects.'
    if fail:
        print(normal_output + ' ' + fail)
    else:
        output(field1, 1, f1)
        output(field2, 2, f2)

