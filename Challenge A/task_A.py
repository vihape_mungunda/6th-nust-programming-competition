import string
while True:
    correct_name = False
    full_name = input("Enter full name: ")
    if len([x for x in full_name.split(' ') if x != ' ' and x != '']) == 1: # Checking to see if there are consecutive spaces in the name
        print("Name must be in the format of 'Name Surname'")
    else:
        num_of_proper_letters = [x for x in full_name if x in string.ascii_letters] # Stores the count for the amount of actual characters in name
        if len(num_of_proper_letters) == 0: # If there are none, then the name is invalid
            print("Invalid Name, must contain characters")
        else:
            if len([x for x in full_name if x not in string.ascii_letters + ' ']) == 0: # Checks to see if there are characters that are neither a letter nor a space
                correct_name = True # If all letters are ok, then correct_name is True and that will break the loop
            else:
                print("Invalid name, must not contain non-letter or non-space characters")
    if correct_name == True:
        break

while True:
    try:
        age = int(input("Enter age in days: "))
    except ValueError: # To account for any errors that will break the code
        print("Invalid age, try again")
        continue # to iterate through the next loop if invalid
    if age < 0: # To account for negative inputs
        print("No negative numbers accepted, try again")
    else:
        break

age_in_years = age // 365 # Stores the age in years
year_born = 2021 - int(age_in_years) # Calculates year born
if age_in_years > 10 : # To recognize decades instead of lutrums
    decades = age_in_years / 10
    exact_decades = age_in_years // 10
    if decades - exact_decades > 0: # To check whether 'over {decades} decades is needed or an exact decade'
        print(
            f'Good day {full_name}, you are {int(age_in_years)} years old. That means that you have lived for over {int(exact_decades)} decade(s) and you were born in {year_born}')
    else:
        print(
            f'Good day {full_name}, you are {int(age_in_years)} years old. That means that you have lived for {int(exact_decades)} decade(s) and you were born in {year_born}')


elif age_in_years < 5: # If age is neither a lustrum nor a decade
    print(f'Good day {full_name}, you are {int(age_in_years)} years old. That means that you have lived for {int(age_in_years)} years and you were born in {year_born}')
elif 5 < age_in_years < 10: # If age is in between 5 and 10, then we're working with lustrums
    lustrums = age_in_years / 5
    exact_lustrums = age_in_years // 5
    if lustrums - exact_lustrums > 0: # To check whether it's bigger than one lustrum e.g 6 years is 1.2 lustrums, which is over one lustrum
        print(f'Good day {full_name}, you are {int(age_in_years)} years old. That means that you have lived for over {int(exact_lustrums)} lustrum and you were born in {year_born}')
    else:
        print(f'Good day {full_name}, you are {int(age_in_years)} years old. That means that you have lived for {int(exact_lustrums)} lustrum and you were born in {year_born}')
elif age_in_years == 5:
    print(f'Good day {full_name}, you are {int(age_in_years)} years old. That means that you have lived for 1 lustrum and you were born in {year_born}')
elif age_in_years == 10:
    print(f'Good day {full_name}, you are {int(age_in_years)} years old. That means that you have lived for 1 decade and you were born in {year_born}')
