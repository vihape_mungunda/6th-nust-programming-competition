
que_num = int(input("Enter number of questions on quiz: "))
total_grade = que_num
answer_keys = []
for x in range(que_num): #allows teacher to enter the answer key
    answer = input(f"Enter answer for question {x + 1}: ")
    answer_keys.append(answer)
learners = {} # creates empty dictionary to store the id number and the number of correct questions for each student e.g. 1234: 5
learn_num = int(input("Enter number of learners: "))
for x in range(learn_num): #for loop will iterate for each learner
    print(f'\nLearner {x + 1}')
    id = input(f"Enter ID: ") #learner enters their id number
    learners[id] = 0 #setting studnt's grade count to 0 (the count corresponding to the student id entered is set to 0)
    for y in range(que_num): #student is repeatedly prompted to enter their answer for each question
        learner_answer = input(f"Enter answer for question {y + 1}: ")
        if answer_keys[y] == learner_answer: #determines whether ansswer is correct
            learners[id] += 1 #if so, student's grade count is incremented by 1


while True:
    sorts = "LEARNER_ID_ASC, LEARNER_ID_DESC, GRADE_ASC, GRADE_DESC"
    print(f"The sorting arrangments are: {sorts}")
    sort = input("Enter sorting arrangement: ")  # allows teacher to choose sorting arrangement
    if sort not in sorts.split(', '):
        print('Invalid arrangement, try again')
        continue
    if sort == 'GRADE_DESC':
        sorted_learners = dict(sorted(learners.items(), key=lambda item: item[1], reverse=True))#sorts the student results in decending order of grades, ensuring that students wit the same grades are sorted in ascending order of id number
    elif sort == 'GRADE_ASC':
        sorted_learners = dict(sorted(learners.items(), key=lambda item: item[1]))#sort by ascending order of grades
    elif sort == 'LEARNER_ID_ASC':
        sorted_learners = dict(sorted(learners.items(), key=lambda item: item[0]))#sort by ascending order of id numbers
    elif sort == 'LEARNER_ID_DESC':
        sorted_learners = dict(sorted(learners.items(), key=lambda item: item[0], reverse=True))#sorts by descending order of learner id numbers
    break

print(sort)
print('\n'.join([f'{x}: {sorted_learners[x]}' for x in sorted_learners])) # Outputs id along with learner grade count on single line for each learner e.g 0001: 5